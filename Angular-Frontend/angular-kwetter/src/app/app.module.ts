import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http'
import {FormsModule,ReactiveFormsModule} from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { KwetterComponent } from './components/kwetter/kwetter.component';
import { KweetItemComponent } from './components/kweet-item/kweet-item.component';
import { SendKweetComponent } from './components/send-kweet/send-kweet.component';
import { YourPageComponent } from './components/your-page/your-page.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { RegisterComponent } from './components/register/register.component';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { JwtInterceptor } from './components/helper/jwt.interceptor';
import { DatePipe } from '@angular/common';
import { FollowComponent } from './components/follow/follow.component';



@NgModule({
  declarations: [
    AppComponent,
    KwetterComponent,
    KweetItemComponent,
    SendKweetComponent,
    YourPageComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    TopBarComponent,
    FollowComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
     { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
