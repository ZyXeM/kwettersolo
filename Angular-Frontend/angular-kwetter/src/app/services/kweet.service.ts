import { Injectable } from '@angular/core';
import {Observable} from 'rxjs'
import {HttpClient,HttpHeaders} from '@angular/common/http'
import {Kweet} from '../models/Kweet'
import {Following} from '../models/Following'
import { environment } from "../environments/environment";


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class KweetService {

  constructor(private http:HttpClient) { }


  getKweets():Observable<Kweet[]> {

  return this.http.get<Kweet[]>(`${environment.kweetUrl}/kweet/gettimeline`);
  }

  sendKweet(kweet:Kweet):Observable<any>  {

   return this.http.post<Kweet>(`${environment.kweetUrl}/kweet/kweet`,kweet,httpOptions);
  }

  getOwnKweets():Observable<Kweet[]> {
     return this.http.get<Kweet[]>(`${environment.kweetUrl}/kweet/getownkweets`);
  }

  getFollowing():Observable<Kweet[]> {
     return this.http.get<Kweet[]>(`${environment.kweetUrl}/kweet/getfollowing`);
  }

  getFollowers():Observable<Kweet[]> {
     return this.http.get<Kweet[]>(`${environment.kweetUrl}/kweet/getfollowers`);
  }

  followUser(username:Following): Observable<any>{
    return this.http.post<any>(`${environment.kweetUrl}/kweet/follow`,username,httpOptions);
  }

}
