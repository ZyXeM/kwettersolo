﻿import { User } from "../models/User";
﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from "../environments/environment";
import { Router } from "@angular/router";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;

  constructor(private router: Router,
              private http: HttpClient) {

    this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
    this.user = this.userSubject.asObservable();
  }

   json: string;

  public get userValue(): User {
    return this.userSubject.value;
  }

  login(username, password) {
    return this.http.post<any>(`${environment.authUrl}/users/login`, {username: username, password: password})
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('user', JSON.stringify(user));
        this.userSubject.next(user);
        return user;
      }));
  }

  deleteAccount() {
      return this.http.post<any>(`${environment.authUrl}/users/delete`,{})
        .subscribe(data => {
            this.logout();
        })
    }

  logout() {
    // remove user from local storage and set current user to null
    localStorage.removeItem('user');
    this.userSubject.next(null);
    this.router.navigate(['/login']);
  }

    register(user: User) {
      this.json = JSON.stringify(user);
      return this.http.post(`${environment.authUrl}/users/sign-up`, this.json, httpOptions);
    }
}
