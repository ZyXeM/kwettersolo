import { Component, OnInit,Input } from '@angular/core';
import {Kweet} from '../../models/Kweet'
@Component({
  selector: 'app-kweet-item',
  templateUrl: './kweet-item.component.html',
  styleUrls: ['./kweet-item.component.css']
})
export class KweetItemComponent implements OnInit {

  @Input() kweet: Kweet;
  constructor() { }

  ngOnInit(): void {
  }

}
