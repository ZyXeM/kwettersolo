import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KweetItemComponent } from './kweet-item.component';

describe('KweetItemComponent', () => {
  let component: KweetItemComponent;
  let fixture: ComponentFixture<KweetItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KweetItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KweetItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
