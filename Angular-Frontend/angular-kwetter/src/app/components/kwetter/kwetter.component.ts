import { Component, OnInit } from '@angular/core';
import {Kweet} from '../../models/Kweet'
import {KweetService} from '../../services/kweet.service'

@Component({
  selector: 'app-kwetter',
  templateUrl: './kwetter.component.html',
  styleUrls: ['./kwetter.component.css']
})
export class KwetterComponent implements OnInit {
  kweets:Kweet[];
  constructor(private kweetService:KweetService) { }

  ngOnInit(): void {
    this.refreshKweet();
  }

  refreshKweet(){
     this.kweetService.getKweets().subscribe(kweets => {
          this.kweets = kweets;
        });
  }

}
