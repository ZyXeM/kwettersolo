import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import {KweetService} from '../../services/kweet.service';
import { FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'app-follow',
  templateUrl: './follow.component.html',
  styleUrls: ['./follow.component.css']
})
export class FollowComponent implements OnInit {

  constructor(private kweetService:KweetService) { }
 text:string;
  @Output() kweet = new EventEmitter();
  ngOnInit(): void {
  }

   onSubmit() {
   const Following = {
         username: this.text
       }
      this.kweetService.followUser(Following).subscribe(data => {
         this.kweet.emit();
         });
    }

}
