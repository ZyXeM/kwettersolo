import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from "../../services/authentication.service";
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-your-page',
  templateUrl: './your-page.component.html',
  styleUrls: ['./your-page.component.css']
})
export class YourPageComponent implements OnInit {

  text:string;
  constructor(    private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
  }

    onSubmit() {

      this.authenticationService.deleteAccount();

    }

}
