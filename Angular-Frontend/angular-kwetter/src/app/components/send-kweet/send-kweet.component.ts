import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';
import {KweetService} from '../../services/kweet.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-send-kweet',
  templateUrl: './send-kweet.component.html',
  styleUrls: ['./send-kweet.component.css']
})
export class SendKweetComponent implements OnInit {
  text:string;
  @Output() kweet = new EventEmitter();
  constructor(private kweetService:KweetService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    const kweet = {
      text: this.text
    }
    this.kweetService.sendKweet(kweet).subscribe(data => {
      this.kweet.emit();
    });

  }

}
