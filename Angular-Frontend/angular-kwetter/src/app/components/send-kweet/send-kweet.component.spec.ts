import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendKweetComponent } from './send-kweet.component';

describe('SendKweetComponent', () => {
  let component: SendKweetComponent;
  let fixture: ComponentFixture<SendKweetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendKweetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendKweetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
