import {UserName} from './UserName'

export class Kweet {
  id?:number;
  text:string;
  date?:string;
  userId?:UserName;

}
