package com.kwetter.solo.Models;

public class MessageKafka {
    private String message;
    private int ownerID;
    private String ownerName;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public MessageKafka() {
    }

    public MessageKafka(String message, int ownerID, String ownerName) {
        this.message = message;
        this.ownerID = ownerID;
        this.ownerName = ownerName;
    }
}
