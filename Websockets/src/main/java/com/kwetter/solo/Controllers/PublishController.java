package com.kwetter.solo.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PublishController {

    @Autowired
    KafkaTemplate<String,com.kwetter.solo.Models.MessageKafka> kafkaKafkaTemplate;

    private String topic = "Kwetter";
    @PostMapping("publish")
    public  String publish(){
        System.out.println("Send!");
        kafkaKafkaTemplate.send(topic,new com.kwetter.solo.Models.MessageKafka("Message!!",1,"Mitch"));

        return "Test";
    }

}
