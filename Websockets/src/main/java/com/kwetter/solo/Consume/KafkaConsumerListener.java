package com.kwetter.solo.Consume;


import com.kwetter.solo.Models.MessageKafka;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;



@Service
public class KafkaConsumerListener {

    @Autowired
    SimpMessagingTemplate template;

    @KafkaListener(topics = "Kwetter" , groupId = "Client", containerFactory = "KafkaMessageListenerContainerFactory")
    public void consume(MessageKafka messageKafka){
        System.out.println("Recieved");
        System.out.println(messageKafka.getMessage());
       // template.convertAndSend("/topic/temperature", messageKafka);
    }
}
