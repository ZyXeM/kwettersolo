package io.mitch.authorizationserver.controller;


import io.mitch.authorizationserver.dao.UsersDao;
import io.mitch.authorizationserver.entity.AuthoritiesEntity;
import Messages.UserModel;
import io.mitch.authorizationserver.entity.TokenResponse;
import io.mitch.authorizationserver.entity.UsersEntity;
import io.mitch.authorizationserver.service.Authority;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.SocketUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.security.Principal;
import java.util.HashSet;

@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserController {
    private UsersDao applicationUserRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Value("${LocalAddress}")
    private String localAddress;

    @Autowired
    private RestTemplate template;

    @Autowired
    KafkaTemplate<String,UserModel> kafkaKafkaTemplate;

    public UserController(UsersDao applicationUserRepository,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.applicationUserRepository = applicationUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PostMapping("/sign-up")
    public void signUp(@RequestBody UsersEntity user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setAccountNonExpired(true);
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);
        HashSet<AuthoritiesEntity> set = new HashSet<>();
        set.add(new AuthoritiesEntity(user,Authority.ROLE_ADMIN));
        user.setAuthorities(set);
        user = applicationUserRepository.save(user);
        if(user != null){
            try {
                kafkaKafkaTemplate.send("Kwetter",new UserModel(user.getUsername(),"email"));
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }

        }


    }

    @PostMapping("/login")
    public TokenResponse login(@RequestBody UsersEntity usersEntity){
        HttpHeaders header = createHeaders("clientId","client-secret");
        MultiValueMap<String,String> map = new LinkedMultiValueMap<String,String>();
        map.add("username",usersEntity.getUsername());
        map.add("password",usersEntity.getPassword());
        map.add("grant_type","password");
        HttpEntity<MultiValueMap<String,String>> request = new HttpEntity<MultiValueMap<String, String>>(map,header);
        ResponseEntity<TokenResponse> response = template.postForEntity(localAddress +"/oauth/token",request,TokenResponse.class);
        return response.getBody();
    }

    @PostMapping("/delete")
    public HttpStatus deleteAccount(Principal principal){
        applicationUserRepository.delete(applicationUserRepository.findByUsername(principal.getName()));
        try {
            kafkaKafkaTemplate.send("Kwetter",new UserModel(principal.getName(),"email",true));
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return HttpStatus.NOT_MODIFIED;
        }
        return HttpStatus.OK;
    }

    @PostMapping("/refresh")
    public TokenResponse refresh(@RequestParam String refreshToken){
        HttpHeaders header = createHeaders("clientId","client-secret");
        MultiValueMap<String,String> map = new LinkedMultiValueMap<String,String>();
        map.add("refresh_token",refreshToken);
        map.add("grant_type","refresh_token");
        HttpEntity<MultiValueMap<String,String>> request = new HttpEntity<MultiValueMap<String, String>>(map,header);
        ResponseEntity<TokenResponse> response = template.postForEntity(localAddress+"/oauth/token",request,TokenResponse.class);
        return response.getBody();
    }

    HttpHeaders createHeaders(String username, String password){
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName("US-ASCII")) );
            String authHeader = "Basic " + new String( encodedAuth );
            set( "Authorization", authHeader );
        }};
    }



    @PreAuthorize("hasRole('USER')")
    @GetMapping("/OK")
    public String OK(Principal principal){
        return principal.getName();
    }



    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/OKSecured")
    public String OKSec(Principal principal){
        return principal.getName();
    }

    @GetMapping("/Unsecure")
    public String unsecure(Principal principal){
        return principal.getName();
    }






}
