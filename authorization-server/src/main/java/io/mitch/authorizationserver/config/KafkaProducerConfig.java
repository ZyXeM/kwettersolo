package io.mitch.authorizationserver.config;


import Messages.UserModel;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import java.util.HashMap;
import java.util.Map;


@Configuration
public class KafkaProducerConfig {
    @Value("${KafkaAddress}")
    private String kafkaIP;
    @Profile("!test")
    @Bean
    public ProducerFactory<String, UserModel> producerFactory() {
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(
                ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                kafkaIP);
           configProps.put(
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class);
        configProps.put(
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                org.springframework.kafka.support.serializer.JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(configProps);
    }
    @Profile("!test")
    @Bean
    public KafkaTemplate<String, UserModel> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }

    @Profile("test")
    @Bean
    KafkaTemplate<String, UserModel> kafkaTemplateMock(){
        return  Mockito.mock(KafkaTemplate.class);
    }

}