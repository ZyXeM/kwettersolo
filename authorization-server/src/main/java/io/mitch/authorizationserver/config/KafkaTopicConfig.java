package io.mitch.authorizationserver.config;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;


@Configuration
public class KafkaTopicConfig {

    @Value("${KafkaAddress}")
    private String kafkaIP;

    @Profile("!prod")
    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaIP);
        return new KafkaAdmin(configs);
    }

    @Profile("!prod")
    @Bean
    public NewTopic topic1() {
        return new NewTopic("Kwetter", 1, (short) 1);
    }
}