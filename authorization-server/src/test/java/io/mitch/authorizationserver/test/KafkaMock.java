package io.mitch.authorizationserver.test;

import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.Message;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.List;
import java.util.Map;

public class KafkaMock <K,V> implements KafkaOperations<K, V> {
    @Override
    public ListenableFuture<SendResult<K, V>> sendDefault(V v) {
        return null;
    }

    @Override
    public ListenableFuture<SendResult<K, V>> sendDefault(K k, V v) {
        return null;
    }

    @Override
    public ListenableFuture<SendResult<K, V>> sendDefault(Integer integer, K k, V v) {
        return null;
    }

    @Override
    public ListenableFuture<SendResult<K, V>> sendDefault(Integer integer, Long aLong, K k, V v) {
        return null;
    }

    @Override
    public ListenableFuture<SendResult<K, V>> send(String s, V v) {
        return null;
    }

    @Override
    public ListenableFuture<SendResult<K, V>> send(String s, K k, V v) {
        return null;
    }

    @Override
    public ListenableFuture<SendResult<K, V>> send(String s, Integer integer, K k, V v) {
        return null;
    }

    @Override
    public ListenableFuture<SendResult<K, V>> send(String s, Integer integer, Long aLong, K k, V v) {
        return null;
    }

    @Override
    public ListenableFuture<SendResult<K, V>> send(ProducerRecord<K, V> producerRecord) {
        return null;
    }

    @Override
    public ListenableFuture<SendResult<K, V>> send(Message<?> message) {
        return null;
    }

    @Override
    public List<PartitionInfo> partitionsFor(String s) {
        return null;
    }

    @Override
    public Map<MetricName, ? extends Metric> metrics() {
        return null;
    }

    @Override
    public <T> T execute(ProducerCallback<K, V, T> producerCallback) {
        return null;
    }

    @Override
    public <T> T executeInTransaction(OperationsCallback<K, V, T> operationsCallback) {
        return null;
    }

    @Override
    public void flush() {

    }

    @Override
    public void sendOffsetsToTransaction(Map<TopicPartition, OffsetAndMetadata> map) {

    }

    @Override
    public void sendOffsetsToTransaction(Map<TopicPartition, OffsetAndMetadata> map, String s) {

    }
}
