package io.mitch.authorizationserver;

import Messages.UserModel;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.client.RestTemplate;

@Configuration
@Profile("test")
public class config {
//    @Bean
//    @Primary
//    KafkaTemplate<String, UserModel> kafkaTemplateMock(){
//        return  Mockito.mock(KafkaTemplate.class);
//    }
    @Autowired
   private TestRestTemplate test;

    @Bean
    @Primary
    RestTemplate template(){
        return  test.getRestTemplate();
    }


}
