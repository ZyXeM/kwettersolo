package com.tweetService.tweetService.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.Clock;
import java.util.Date;

@Entity
public class Kweet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String text;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="CET")
    private Date date;

    @ManyToOne
    @JoinColumn(name = "username")
    private UsersEntity username;

    public Kweet(String text, UsersEntity userId, String username) {
        this.text = text;
        this.username = userId;
    }

    public Kweet() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public UsersEntity getUserId() {
        return username;
    }

    public void setUserId(UsersEntity userId) {
        this.username = userId;
    }

}
