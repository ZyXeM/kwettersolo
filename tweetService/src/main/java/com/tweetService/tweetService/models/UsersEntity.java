package com.tweetService.tweetService.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name= "users")
@JsonIgnoreProperties({"follows"})
public class UsersEntity {



    public UsersEntity() {
    }
    @Id
    @Column(columnDefinition = "VARCHAR(15)")
    private String username;

    public Set<UsersEntity> getFollows() {
        return follows;
    }

    public void setFollows(Set<UsersEntity> follows) {
        this.follows = follows;
    }


    @ManyToMany(fetch = FetchType.EAGER)
   // @JoinColumn(name = "username")
    @JsonIgnore
    private Set<UsersEntity> follows;


    public UsersEntity(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }





}
