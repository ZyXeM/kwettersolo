package com.tweetService.tweetService.models;

public class Follow {
    public Follow(String username) {
        this.username = username;
    }

    public Follow() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String username;
}
