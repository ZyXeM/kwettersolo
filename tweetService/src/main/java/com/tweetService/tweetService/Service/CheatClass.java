package com.tweetService.tweetService.Service;

import com.tweetService.tweetService.models.Kweet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@Service
public class CheatClass {
    @Autowired
    EntityManager entityManager;
    public List<Kweet> getTimeline() {

        TypedQuery<Kweet> query
                = entityManager.createQuery("SELECT k FROM Kweet k where k.username in (select u.follows_username from u.users_follows u where u.users_entity_username = Mitch1) ", Kweet.class);
         List<Kweet> resultList = query.getResultList();
        return resultList;
    }
}
