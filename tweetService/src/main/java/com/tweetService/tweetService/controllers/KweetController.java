package com.tweetService.tweetService.controllers;


import com.tweetService.tweetService.Service.CheatClass;
import com.tweetService.tweetService.dao.KweetDao;
import com.tweetService.tweetService.dao.UsersDao;
import com.tweetService.tweetService.models.Follow;
import com.tweetService.tweetService.models.Kweet;

import com.tweetService.tweetService.models.UsersEntity;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/kweet")
public class KweetController {

    private String profAdress = "https://us-central1-soloproject-278311.cloudfunctions.net/prof";

    @Autowired
    private KweetDao kweetDao;
    @Autowired
    private UsersDao usersDao;
//    @Autowired
//    private KoppelDao koppelDao;
    @Autowired
    private CheatClass cheatClass;


    @GetMapping("/gettimeline")
    public List<Kweet> getTimeline( Principal principal){
        return  kweetDao.findByPreference(principal.getName());
    }

    @GetMapping("/getownkweets")
    public List<Kweet> getOwnKweets( Principal principal){
        return  kweetDao.findByUsername(principal.getName());
    }

    @GetMapping("/getmentions")
    public List<Kweet> getMentions(){
        return new ArrayList<Kweet>();
    }



    @GetMapping("/getfollowing")
    public List<Kweet> getFollowing(Principal principal){
        return usersDao.findFollowing(principal.getName());
    }

    @GetMapping("/getfollowers")
    public List<Kweet> getFollowers(Principal principal){
        return usersDao.findFollowers(principal.getName());
    }


    @PostMapping("/kweet")
    public HttpStatus postKweet(@RequestBody Kweet kweet, HttpServletRequest httpServletRequest) throws JSONException {
        if(kweet.getText().length() > 140){
            return HttpStatus.BAD_REQUEST;
        }

        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject personJsonObject = new JSONObject();
        personJsonObject.put("message",kweet.getText());

        HttpEntity<String> entity = new HttpEntity<String>(personJsonObject.toString(), headers);
        ResponseEntity<String> response = template.postForEntity(profAdress, entity,String.class);
        if(response.getStatusCode() ==HttpStatus.OK)
            kweet.setText(response.getBody()); 

       UsersEntity user = usersDao.findByUsername(httpServletRequest.getUserPrincipal().getName());
       kweet.setUserId(user);
       kweet.setDate(new Date());
       kweetDao.save(kweet);
       return  HttpStatus.OK;
    }

    @PostMapping("/follow")
    public HttpStatus follow(@RequestBody Follow username, Principal principal){
        UsersEntity following = usersDao.findByUsername(username.getUsername());
        if(following != null){
            UsersEntity follower = usersDao.findByUsername(principal.getName());
            follower.getFollows().add(following);
            usersDao.save(follower);
            return HttpStatus.OK;
        }
        return HttpStatus.EXPECTATION_FAILED;

    }


}
