package com.tweetService.tweetService.services;


import Messages.UserModel;
import com.tweetService.tweetService.dao.KweetDao;
import com.tweetService.tweetService.dao.UsersDao;
import com.tweetService.tweetService.models.UsersEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Profile("!test")
@Service
public class KafkaConsumerListener {
    @Autowired
    private UsersDao userDao;
    @Autowired
    private KweetDao kweetDao;



    @KafkaListener(topics = "Kwetter" , groupId = "Client", containerFactory = "KafkaMessageListenerContainerFactory")
    public void consume(UserModel userModel){
        if(userModel.isDelete()){
            kweetDao.deleteKweets(userModel.getUsername());
            userDao.deleteBond(userModel.getUsername());
            userDao.delete(userDao.findByUsername(userModel.getUsername()));
        }else{
            UsersEntity entity = new UsersEntity(userModel.getUsername());
            userDao.save(entity);
        }

    }
}