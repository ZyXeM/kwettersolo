package com.tweetService.tweetService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
public class TweetServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(TweetServiceApplication.class, args);
	}
}
