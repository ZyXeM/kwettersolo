package com.tweetService.tweetService.dao;


import com.tweetService.tweetService.models.Kweet;
import com.tweetService.tweetService.models.UsersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface UsersDao extends JpaRepository<UsersEntity, Long> {

    UsersEntity findByUsername(String username);

    public final static String getFollowers = "Select * from users u where u.username in (SELECT u.follows_username FROM users_follows u where u.users_entity_username = :username)";

    @Query(value = getFollowers, nativeQuery = true)
    List<Kweet> findFollowers(@Param("username") String username);

    public final static String getFollowing = "Select * from users u where u.username in (SELECT u.users_entity_username FROM users_follows u where u.follows_username = :username)";

    @Query(value = getFollowing, nativeQuery = true)
    List<Kweet> findFollowing(@Param("username") String username);




    public final static String deleteBond = "delete from users_follows u where u.users_entity_username = :username";
    @Modifying
    @Transactional
    @Query(value = deleteBond, nativeQuery = true)
    void deleteBond(@Param("username") String username);
}
