package com.tweetService.tweetService.dao;

import com.tweetService.tweetService.models.Kweet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface KweetDao extends JpaRepository<Kweet, Long> {
    List<Kweet> findByUsername(String username);

    public final static String getTimeline = "SELECT * FROM kweet k where k.username in (SELECT u.follows_username FROM users_follows u where u.users_entity_username = :username) OR k.username = :username order by date desc";

    @Query(value = getTimeline, nativeQuery = true)
    List<Kweet> findByPreference(@Param("username") String username);


    public final static String deleteKweets = "delete from kweet k where k.username = :username";
    @Modifying
    @Transactional
    @Query(value = deleteKweets, nativeQuery = true)
    void deleteKweets(@Param("username") String username);
}
